FROM python:3
WORKDIR /code
COPY . /code
RUN mkdir -p db
run pip install -r requirements.txt
EXPOSE 8000
cmd sh init.sh && python3 manage.py runserver 0.0.0.0:8000
